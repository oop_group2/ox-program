/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.waratchaya.ox;

import java.util.Random;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Arrays;

/**
 *
 * @author Melon
 */
class OXFunction {

    protected String[][] arr = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    String whoplay = "?";
    int row = 0;

    public OXFunction() {
        System.out.println("Hello Welcome to OX GAME><!");

        Random row = new Random();
        if (row.nextInt(1) == 0) {
            whoplay = "O";
        } else {
            whoplay = "X";
        }

        for (int i = 0; i < 3; i++) {
            System.out.print(" \n");
            for (int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + "   ");
            }
        }
        System.out.println("\nTurn  " + whoplay);
        System.out.println("Please input row, col");
    }

    public void input() {
        Scanner kb = new Scanner(System.in);
        checkWinner();
        int o = 0;
        int x = 0;

        if (row >= 9) {
            System.out.println("Draw!");
            System.exit(0);
        }
        try {
            o = kb.nextInt();
            x = kb.nextInt();

            // System.out.print(o + "  ");
            //System.out.print(x + "\n");
        } catch (InputMismatchException e) {
            System.out.println("Please Input Number ;^;");
            input();
            return;
        }

        if (o > 3 || o < 1 || x > 3 || x < 1) {
            System.out.println(" O or X greater than 3 or less than 0 !");
            input();
            return;
        }

        String check = arr[o - 1][x - 1];
        if (check.equals("-")) {
            arr[o - 1][x - 1] = whoplay;
        } else {
            System.out.println("Your input is overlapped from another player");
            input();
            return;
        }

        ChangeTeuPlayer();
        row++;
        System.out.println("\nTurn  " + whoplay);
        System.out.println("Please input row, col");
    }

    public void ChangeTeuPlayer() {
        if (whoplay.equals("O")) {
            whoplay = "X";
        } else {
            whoplay = "O";
        }
        printTable();
    }

    public void printTable() {
        for (int i = 0; i < 3; i++) {
            System.out.print(" \n");
            for (int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + "   ");
            }
        }
    }

    public void checkWinner() { 
        
        for (int i = 0; i < 3; i++){ // 1 5 9
            String checkZ1 = arr[2][0];
            String checkZ2 = arr[1][1];
            String checkZ3 = arr[0][2];
            if (checkZ1.equals(checkZ2) && checkZ2.equals(checkZ3)){
                if((!checkZ1.equals("-") && !checkZ2.equals("-")) && !checkZ3.equals("-") == true){
                    System.out.println(checkZ1 + " is Winner Z");
                    System.exit(0);

                }
            }
        }
        
        for (int i = 0; i < 3; i++){ // 3 5 7
            String checkZ1 = arr[2][0];
            String checkZ2 = arr[1][1];
            String checkZ3 = arr[0][2];
            if(checkZ1.equals(checkZ2) && checkZ2.equals(checkZ3)){
                if((!checkZ1.equals("-") && !checkZ2.equals("-")) && !checkZ3.equals("-") == true){
                    System.out.println(checkZ1 + " is Winner");
                    System.exit(0);
                }
            }
        }
        
        for (int i = 0; i < 3; i++) { //CheckY
            for (int j = 0; j < 3; j++) {
                String checkY1 = arr[0][i];
                String checkY2 = arr[1][i];
                String checkY3 = arr[2][i];
                if (checkY1.equals(checkY2) && checkY2.equals(checkY3)) {
                    if ((!checkY1.equals("-") && !checkY2.equals("-")) && !checkY3.equals("-") == true) {
                        System.out.println(checkY1 + " is Winner");
                        System.exit(0);
                    }
                }
            }
        }
        
        for (int i = 0; i < 3; i++){  //CheckX
            for (int j = 0; j < 3; j++){
                String checkX1 = arr[i][0];
                String checkX2 = arr[i][1];
                String checkX3 = arr[i][2];
                if(checkX1.equals(checkX2) && checkX2.equals(checkX3)){
                    if((!checkX1.equals("-") && !checkX2.equals("-")) && !checkX3.equals("-")){
                        System.out.println(checkX1 + " is Winner");
                        System.exit(0);

                    }
                }
                else{
                    
                }
            }
        }
    }
}
